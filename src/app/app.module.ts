import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { MainComponent } from './components/main/main.component';
import { LoginComponent } from './components/login/login.component';
import { OverviewComponent } from './components/overview/overview.component';

import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';

import { LoginAuthenticationService } from './services/loginauthentication.service';
import { RouterManagerService } from './services/routehandler.service';
import { CookieService } from 'angular2-cookie/services/cookies.service';



@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    OverviewComponent,

  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      { path: '', component: OverviewComponent },
      { path: 'login', component: LoginComponent }
    ]),
    HttpModule
  ],
  providers: [
    LoginAuthenticationService,
    RouterManagerService,
    CookieService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
