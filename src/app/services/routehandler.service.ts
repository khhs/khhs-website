import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable()
export class RouterManagerService {

  constructor(private routerservice:Router){

  }

  routeToOverview(){
    this.routerservice.navigateByUrl("/");
  }

  routeToLogin(){
    this.routerservice.navigateByUrl("/login");
  }

}
