import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Account } from '../model/Account';
import { TokenState } from '../model/TokenState';
import { Token } from '../model/Token';

@Injectable()
export class LoginAuthenticationService {

  HttpClient: Http;

  constructor(http: Http) {
    this.HttpClient = http;

  }

  public checkToken(): TokenState {
    const acc: Account = JSON.parse(localStorage.getItem('creds'));
    if (acc != null) {
      this.checkExpiration(acc.Token);
    }
    return TokenState.NoToken;
  }

  public checkExpiration(token: Token) {
    if (token.Expires) {
      
    }
  }

  public loginUser(username: String, password: String) {
    const test = this.HttpClient.post('http://localhost:64494/api/account/login',  { Username: username, Password: password })
    .toPromise( )
    .then(value => {
      if (value.ok) {
        const body = value.json();
        if (body === false) {
          // Wrong login
        } else {
          localStorage.setItem('creds', body + '');
        }
      }
    });
  }

}
