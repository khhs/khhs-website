export class TokenState{
    public static Expired = 0;
    public static Valid = 1;
    public static NoToken = 2;
}