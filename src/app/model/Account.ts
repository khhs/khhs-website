import { Token } from './Token';

export class Account{
    public AccountId: Number;
    public Username: String;
    public Password: String;
    public Email: String;
    public Firstname: String;
    public Lastname: String;
    public ProfileType: Number;
    public TimeCreated: Number;
    public Token: Token;

    constructor(){
        
    }
}