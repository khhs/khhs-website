export class Token{
    public AuthString: String;
    public Expires: Number;
    public AccountId: Number;
}