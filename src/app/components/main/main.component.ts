import { Component, OnInit } from '@angular/core';

import { RouterManagerService } from '../../services/routehandler.service';
import { LoginAuthenticationService } from '../../services/loginauthentication.service';

@Component({
  selector: 'app-main',
  templateUrl: 'main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  constructor(private router: RouterManagerService, private authService: LoginAuthenticationService) {

    if (authService.checkToken()) {
      router.routeToLogin();
      router.routeToOverview();
    }

  }

  ngOnInit() {
    
  }

}
