import { Component, OnInit } from '@angular/core';
import { LoginAuthenticationService } from '../../services/loginauthentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  LoginWorker: LoginAuthenticationService;

  constructor(private loginWorker: LoginAuthenticationService) {
    this.LoginWorker = loginWorker;
  }

  ngOnInit() {
  }

  public loginbutton_click(username:String, password:String){
    this.LoginWorker.loginUser(username, password);
  }

}
